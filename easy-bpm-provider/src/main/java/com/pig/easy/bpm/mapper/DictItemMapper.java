package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.request.DictItemQueryDTO;
import com.pig.easy.bpm.dto.response.DictItemDTO;
import com.pig.easy.bpm.entity.DictItemDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典详细表 Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
@Mapper
public interface DictItemMapper extends BaseMapper<DictItemDO> {

    List<DictItemDTO> getListByCondition(DictItemQueryDTO dictItemQueryDTO);

    List<DictItemDTO> getListByDictCode(@Param("dictCode") String dictCode);
}
