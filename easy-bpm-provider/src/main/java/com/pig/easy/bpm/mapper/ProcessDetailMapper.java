package com.pig.easy.bpm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pig.easy.bpm.dto.response.ProcessDetailDTO;
import com.pig.easy.bpm.entity.ProcessDetailDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-06-06
 */
@Mapper
public interface ProcessDetailMapper extends BaseMapper<ProcessDetailDO> {

    List<ProcessDetailDTO> getListByCondition(ProcessDetailDO processDetail);
}
