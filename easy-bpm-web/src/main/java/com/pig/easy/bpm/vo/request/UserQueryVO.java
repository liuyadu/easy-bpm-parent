package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/31 17:33
 */
@Data
@ToString
public class UserQueryVO implements Serializable {

    private static final long serialVersionUID = 8542325207421763714L;

    private Long userId;

    private List<Long> userIdList;

    private String userName;

    private String realName;

    private String email;

    private String phone;

    private Integer gender;

    private String avatar;

    private Date birthDate;

    private Long companyId;

    private Long deptId;

    private String tenantId;

    private Date entryTime;

    private Date leaveTime;

    private Integer hireStatus;

    private String positionCode;

    private String positionName;

    private int pageIndex;

    private int pageSize;

}
