package com.pig.easy.bpm.context;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.pig.easy.bpm.dto.response.UserInfoDTO;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/15 10:20
 */
public class GlobalUserInfoContext {

    private static final TransmittableThreadLocal<UserInfoDTO> loginInfoThreadLocal = new TransmittableThreadLocal<>();

    public GlobalUserInfoContext() {
    }

    public static UserInfoDTO getLoginInfo(){
        return loginInfoThreadLocal.get();
    }

    public static void setLoginInfo(UserInfoDTO userInfoDTO){
        loginInfoThreadLocal.set(userInfoDTO);
    }

    public static void removeInfo() {
        loginInfoThreadLocal.remove();
    }
}
