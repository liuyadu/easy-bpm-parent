package com.pig.easy.bpm.vo.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 模板文件表
 * </p>
 *
 * @author pig
 * @since 2020-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "模板文件表对象", description = "模板文件表")
public class FileTempleteQueryVO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "GUID主键")
    /**
     * GUID主键
     */
    private String tempalteId;

    @ApiModelProperty(value = "租户编号")
    /**
     * 租户编号
     */
    private String tenantId;


    @ApiModelProperty(value = "流程编号")
    /**
     * 流程编号
     */
    private Long processId;


    @ApiModelProperty(value = "模板文件名称")
    /**
     * 模板文件名称
     */
    private String fileName;


    @ApiModelProperty(value = "模板文件名称")
    /**
     * 模板文件名称
     */
    private String fileMd5Name;


    @ApiModelProperty(value = "文件后缀")
    /**
     * 文件后缀
     */
    private String fileExtend;


    @ApiModelProperty(value = "文件路径")
    /**
     * 文件路径
     */
    private String filePath;


    @ApiModelProperty(value = "文件大小")
    /**
     * 文件大小
     */
    private BigDecimal fileSize;


    @ApiModelProperty(value = "备注")
    /**
     * 备注
     */
    private String remarks;


    @ApiModelProperty(value = "排序值")
    /**
     * 排序值
     */
    private Integer sort;


    @ApiModelProperty(value = "状态 1 有效 0 失效")
    /**
     * 状态 1 有效 0 失效
     */
    private Integer validState;


    @ApiModelProperty(value = "操作人工号")
    /**
     * 操作人工号
     */
    private Long operatorId;


    @ApiModelProperty(value = "操作人姓名")
    /**
     * 操作人姓名
     */
    private String operatorName;


    @ApiModelProperty(value = "创建时间")
    /**
     * 创建时间
     */
    private LocalDateTime createTime;


    @ApiModelProperty(value = "最后一次更新时间")
    /**
     * 最后一次更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 当前页码
     */
    private Integer pageIndex;

    /**
     * 每页展示数量
     */
    private Integer pageSize;


}
