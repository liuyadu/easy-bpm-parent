package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/6 10:13
 */
@Data
@ToString
public class ProcessDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 6226380949612837849L;

    private Long processId;

    private String processKey;

    private String processName;

    private Long processMenuId;

    private String processAbbr;

    private Long companyId;

    private String companyCode;

    private String tenantId;

    private Integer sort;

    private Long processDetailId;

    private Integer processType;

    private Integer processStatus;

    private String remarks;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

}
