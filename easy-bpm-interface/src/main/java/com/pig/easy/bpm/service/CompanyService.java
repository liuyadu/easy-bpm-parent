package com.pig.easy.bpm.service;


import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.CompanyReqDTO;
import com.pig.easy.bpm.dto.response.CompanyDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.dto.response.TreeDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 公司表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
public interface CompanyService{

    Result<PageInfo> getListByCondition(CompanyReqDTO companyReqDTO);

    Result<List<ItemDTO>> getCompanyIdAndNameDictList(CompanyReqDTO companyReqDTO);

    Result<CompanyDTO> getCompanyById(Long companyId);

    Result<CompanyDTO> getCompanyByCode(String companyCode);

    Result<Integer> insert(CompanyReqDTO companyReqDTO);

    @Deprecated
    Result<Integer> updateByCompanyId(CompanyReqDTO companyReqDTO);

    Result<Integer> updateByCompanyCode(CompanyReqDTO companyReqDTO);

    Result<List<TreeDTO>> getCompanyTree(Long parentId, String tenantId);
}
