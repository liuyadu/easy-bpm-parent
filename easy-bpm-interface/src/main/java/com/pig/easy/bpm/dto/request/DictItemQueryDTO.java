package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 18:55
 */
@Data
@ToString
public class DictItemQueryDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -1132751687393016780L;

    private Long itemId;

    private Long dictId;

    private String itemValue;

    private String tenantId;

    private String itemText;

    private Integer sort;

    private String remark;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private int pageSize;

    private int pageIndex;
}
