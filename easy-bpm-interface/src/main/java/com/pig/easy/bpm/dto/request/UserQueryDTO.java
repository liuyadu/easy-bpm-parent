package com.pig.easy.bpm.dto.request;

import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/24 17:59
 */
@Data
@ToString
public class UserQueryDTO extends BaseRequestDTO {

    private static final long serialVersionUID = -9137374897541570940L;

    private Long userId;

    private List<Long> userIdList;

    private String userName;

    private String realName;

    private String email;

    private String phone;

    private Integer gender;

    private String avatar;

    private Date birthDate;

    private Long companyId;

    private Long deptId;

    private String tenantId;

    private Date entryTime;

    private Date leaveTime;

    private Integer hireStatus;

    private String positionCode;

    private String positionName;

    private int pageIndex;

    private int pageSize;

}
