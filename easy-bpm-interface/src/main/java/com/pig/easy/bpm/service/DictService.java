package com.pig.easy.bpm.service;

import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.DictQueryDTO;
import com.pig.easy.bpm.dto.request.DictUpdateDTO;
import com.pig.easy.bpm.dto.response.DictDTO;
import com.pig.easy.bpm.dto.response.ItemDTO;
import com.pig.easy.bpm.utils.Result;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-28
 */
public interface DictService {

    Result<PageInfo> getListByCondition(DictQueryDTO dictQueryDTO);

    Result<DictDTO> getDictByDictCode(String dictCode);

    Result<Integer> insertOrUpdateDict(DictUpdateDTO dictUpdateDTO);

    Result<List<ItemDTO>> getDictListByDictCode(String dictCode);

    Result<List<ItemDTO>> getDict(DictQueryDTO dictQueryDTO);

}
