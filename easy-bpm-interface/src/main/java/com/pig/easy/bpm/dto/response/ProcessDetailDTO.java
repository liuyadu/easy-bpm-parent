package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/8 16:18
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class ProcessDetailDTO extends BaseResponseDTO{

    private static final long serialVersionUID = -1061308321310268614L;

    private Long processDetailId;

    private String tenantId;

    private Long processId;

    private String processXml;

    private String applyTitleRule;

    private Date applyDueDate;

    private String remarks;

    private Long operatorId;

    private String operatorName;

    private Integer publishStatus;

    private Integer mainVersion;

    private String definitionId;

    private Integer autoCompleteFirstNode;

}
